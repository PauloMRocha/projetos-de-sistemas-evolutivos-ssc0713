#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define POP 15
#define MAX_X 140

float funcao(float);
int ranking(float *, int *);
int reproducao(float *, int *);
void elitismo(float *agente, int *rank);
void roleta(float *agente, int *rank);
void mutacao(float *agente, int *rank, float TaxMut);

int main () {
	char aux;
	int gen = 0;
	float mut = 1.50;
	printf("Inicialização do Algoritmo Evolutivo\n");
	printf("Numero de gerações: ");
	scanf("%d", &gen);


	// Inicialização dos 'agentes'
	float agentes[POP], resultados[POP], melhor = 0.00;
	srand(time(NULL));
	for(int i = 0; i < POP; i++){
		agentes[i] = (float) (rand() % (MAX_X * 100))/100.00;
	}

	int iter = 0, rank[POP];

	while(iter<=gen) {
		iter++;
		for(int i = 0; i < POP; i++) {
			resultados[i] = funcao(agentes[i]);
		//	printf("Resultado agente %d: %f\n", i, resultados[i]);
		}
		ranking(agentes, rank);

		if((agentes[rank[0]]-melhor) < 10e-3){
			if(mut*1.5 < 100){
				mut = mut*1.5;
			}
		}else mut = mut/3;

		printf("mut: %f\n", mut);

		melhor = agentes[rank[0]];
		printf("Melhor: %f\n", melhor);
		//reproducao(agentes, rank);
		roleta(agentes, rank);
		//elitismo(agentes, rank);

		mutacao(agentes, rank, mut);
		//scanf("%c", &aux);
	}
}

/**
 * Função objetivo, método de avaliação, deve ser maximizada.
 */
float funcao(float x) {
	// if (x > 10)
	// 	return (20.00 - x)*2;
	// else
	// 	return 2*x;

	return 2*cos(0.39*x) + 5*sin(0.5*x) + 0.5*cos(0.1*x) + 10*sin(0.7*x) + 5*sin(1*x) + 5*sin(0.35*x);
}


/**
 * Função que retorna as posições dos melhores agentes. Isto é, o
 * primeiro valor corresponde à posição do melhor agente.
 */
int ranking(float *vetor, int *rank) {
	struct dupla {
		float valor;
		int pos;
	};

	if(vetor == NULL)
		return -1;

	struct dupla auxiliar[POP];
	for(int i = 0; i < POP; i++) {
		auxiliar[i].valor = funcao(vetor[i]);
		auxiliar[i].pos = i;
	}

	// sorting
	struct dupla aux;
	for(int i = 0; i < POP; i++) {
		for(int j = i + 1; j < POP; j++) {
			if (auxiliar[j].valor > auxiliar[i].valor) {
				aux = auxiliar[i];
				auxiliar[i] = auxiliar[j];
				auxiliar[j] = aux;
			}
		}
	}

	for(int i = 0; i < POP; i++) {
		//printf("Posição %d, valor %f\n", auxiliar[i].pos, auxiliar[i].valor);
		rank[i] = auxiliar[i].pos;
	}
	return 0;
}

/**
 * Função que gera nova geração de agentes.
 */
int reproducao(float *agente, int *rank) {
	float filhos[POP];
	for (int i = 0; i < 5; i++)
		filhos[i] = (agente[rank[0]] + agente[rank[rand() % POP]]) / 2.000;

	for (int i = 0; i < 3; i++)
		filhos[5 + i] = (agente[rank[1]] + agente[rank[rand() % POP]]) / 2.000;

	for (int i = 0; i < 2; i++)
		filhos[8 + i] = (agente[rank[2]] + agente[rank[rand() % POP]]) / 2.000;

	for (int i = 0; i < 5; i++)
		filhos[10 + i] = (agente[rank[3 + i]] + agente[rank[rand() % POP]]) / 2.000;

	for (int i = 0; i < POP; i++)
		agente[i] = filhos[i];
	return 0;
}

void elitismo(float *agente, int *rank){ //media do melhor com todos
	float filhos[POP];
	for (int i = 0; i < POP; i++)
		filhos[i] = (agente[rank[0]] + agente[i]) / 2.000;

	for (int i = 0; i < POP; i++)
		agente[i] = filhos[i];
}

void roleta(float *agente, int *rank){ //esquema de roleta com os 3 mlehores
	float pri = 40.00, seg = 35.00, ter = 25.00;
	float r;
	float filhos[POP];

	for (int i = 0; i < POP; i++){
		r = (rand() % 10000)/100;//gera ujm float entre 0 e 100

		if(i != rank[0]){ //protege o melhor
			if(r > 0 && r <= pri){//o melhor
				filhos[i] = (agente[rank[0]] + agente[i]) / 2.000;

			}else if(r > pri && r <= (seg+pri)){//o segundo
				filhos[i] = (agente[rank[1]] + agente[i]) / 2.000;

			}else if(r > (seg+pri) && r <= (ter+seg+pri)){//o terceiro
				filhos[i] = (agente[rank[2]] + agente[i]) / 2.000;

			}else{//caso as probabilidades nao somem 100 da preferencia para o melhor
				filhos[i] = (agente[rank[0]] + agente[i]) / 2.000;
			}
		}else{
			filhos[i] = agente[i];
 		}
	}

	for (int i = 0; i < POP; i++)
		agente[i] = filhos[i];
}


void mutacao(float *agente, int *rank, float TaxMut){

	for(int i=0; i<POP; i++){
		if(i != rank[0])
			agente[i] = agente[i] + (float) (((rand()%MAX_X - (MAX_X/2.0))/100.0) * TaxMut);
		if(agente[i] > MAX_X || agente[i] < 0)
			agente[i] = (float) (rand() % (MAX_X * 100))/100.00;
	}
}
